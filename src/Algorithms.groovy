import java.security.InvalidKeyException
import java.security.KeyFactory
import java.security.KeyPair
import java.security.KeyPairGenerator
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.security.PrivateKey
import java.security.PublicKey
import java.security.spec.X509EncodedKeySpec
import java.util.Map
import javax.crypto.Cipher
import javax.crypto.KeyAgreement
import javax.crypto.KeyGenerator
import javax.crypto.Mac
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory
import javax.crypto.interfaces.DHPublicKey
import javax.crypto.spec.DESKeySpec
import javax.crypto.spec.DHParameterSpec


class Algorithms implements Runnable{
	static String salt='DGE$5SGr@3VsHYUMas2323E4d57vfBfFSTRU@!DSH(*%FDSdfg13sgfsg'

	byte[] bob, alice;
	byte[] ciphertext;

	boolean doneAlice
	BigInteger aliceP, aliceG;
	int aliceL;

	synchronized void run() {
		if (!doneAlice) {
			doneAlice = true
			doAlice()
		}
		else doBob()
	}
	synchronized void doAlice() {
		try {
			// Alice generates a key pair
			KeyPairGenerator kpg = KeyPairGenerator.getInstance('DH')
			kpg.initialize(1024)
			KeyPair kp = kpg.generateKeyPair()

			// Alice sends the public key and the
			// 	Diffie-Hellman algorithm key parameters to Bob
			Class dhClass = Class.forName(
					'javax.crypto.spec.DHParameterSpec')
			DHParameterSpec dhSpec = (
					(DHPublicKey) kp.getPublic()).getParams()
			aliceG = dhSpec.getG()
			aliceP = dhSpec.getP()
			aliceL = dhSpec.getL()
			alice = kp.getPublic().getEncoded()
			notify()

			//  Alice performs the first phase of the
			//		protocol with her private key
			KeyAgreement ka = KeyAgreement.getInstance('DH')
			ka.init(kp.getPrivate())

			//   Alice performs the second phase of the
			//		protocol with Bob's public key
			while (bob == null) {
				wait();
			}
			KeyFactory kf = KeyFactory.getInstance('DH')
			X509EncodedKeySpec x509Spec = new X509EncodedKeySpec(bob)
			PublicKey pk = kf.generatePublic(x509Spec)
			ka.doPhase(pk, true)

			//   Alice can generate the secret key

			def secret = ka.generateSecret() as byte[]

			// Alice generates a DES key
			// DESKeySpec object using the first 8 bytes in key as the key material for the DES key.
			SecretKeyFactory skf = SecretKeyFactory.getInstance('DES')
			DESKeySpec desSpec = new DESKeySpec(secret)
			SecretKey key = skf.generateSecret(desSpec)

			// Alice encrypts data with the key and sends
			//		the encrypted data to Bob
			Cipher c = Cipher.getInstance('DES/ECB/PKCS5Padding')
			c.init(Cipher.ENCRYPT_MODE, key)
			ciphertext = c.doFinal(
					'Stand and unfold yourself'.getBytes())
			notify()
		} catch (Exception e) {
			e.printStackTrace()
		}
	}

	public synchronized void doBob() {
		try {
			//   Bob uses the parameters supplied by Alice
			//		to generate a key pair and sends the public key
			while (alice == null) {
				wait()
			}
			KeyPairGenerator kpg = KeyPairGenerator.getInstance('DH')
			DHParameterSpec dhSpec = new DHParameterSpec(
					aliceP, aliceG, aliceL)
			kpg.initialize(dhSpec)
			KeyPair kp = kpg.generateKeyPair()
			bob = kp.getPublic().getEncoded()
			notify()

			//   Bob uses his private key to perform the
			//		first phase of the protocol
			KeyAgreement ka = KeyAgreement.getInstance('DH')
			ka.init(kp.getPrivate())
			//  Bob uses Alice's public key to perform
			//		the second phase of the protocol.
			KeyFactory kf = KeyFactory.getInstance('DH')
			X509EncodedKeySpec x509Spec =
					new X509EncodedKeySpec(alice)
			PublicKey pk = kf.generatePublic(x509Spec)
			ka.doPhase(pk, true)
			
			//   Bob generates the secret key
			def secret = ka.generateSecret() as byte[]
			// Bob generates a DES key
			SecretKeyFactory skf = SecretKeyFactory.getInstance('DES')
			DESKeySpec desSpec = new DESKeySpec(secret)
			SecretKey key = skf.generateSecret(desSpec)

			// Bob receives the encrypted text and decrypts it
			Cipher c = Cipher.getInstance('DES/ECB/PKCS5Padding')
			c.init(Cipher.DECRYPT_MODE, key)
			while (ciphertext == null) {
				wait()
			}
			def plaintext = c.doFinal(ciphertext) as byte[]

			println("Bob got the string " +
					new String(plaintext))
		} catch (Exception e) {
			e.printStackTrace()
		}
	}

	static main(args) {
		//RSA Algorithm

		String txt = 'Hi..How r u?'
		Map map = getKeys()

		PrivateKey privateKey =  map.get('private')
		PublicKey publicKey =  map.get('public')


		String encryptedText = encryptMessage(txt, publicKey)
		String descryptedText = decryptMessage(encryptedText, privateKey )

		println "input: ${txt}"
		println "encrypted: ${encryptedText}"
		println "decrypted: ${descryptedText}"

		//MD5 Algorithm
		String password = 'thisismypassword'
		String empty =  null
		String msg = 'This is a text message'

		println "${password} MD5 hashed to>>>>>>> : ${md5Hash(password)}"
		println "${empty} MD5 hashed to>>>>>>> : ${md5Hash(empty)}"
		println "${msg} MD5 hashed to>>>>>>> : ${md5Hash(msg)}"

		//Message Authentication Algorithm
		hmacAlgorithm()

		//KeyAgreement Algorithm

		Algorithms test = new Algorithms();
		new Thread(test).start()		// Starts Alice
		new Thread(test).start()		// Starts Bob
	}
	static hmacAlgorithm(){
		try {
			// get a key generator for the HMAC-MD5 keyed-hashing algorithm
			KeyGenerator keyGen = KeyGenerator.getInstance("HmacMD5")
			// generate a key from the generator
			SecretKey key = keyGen.generateKey()
			// create a MAC and initialize with the above key
			Mac mac = Mac.getInstance(key.getAlgorithm())
			mac.init(key)
			String message = "tydfhjjug"
			// get the string as UTF-8 bytes
			byte[] b = message.getBytes("UTF-8")
			// create a digest from the byte array
			byte[] digest = mac.doFinal(b)
			
			println(digest)
		}
		catch (NoSuchAlgorithmException e) {
			println("No Such Algorithm:" + e.getMessage())
			return
		}
		catch (UnsupportedEncodingException e) {
			println("Unsupported Encoding:" + e.getMessage())
			return
		}
		catch (InvalidKeyException e) {
			println('Invalid Key:' + e.getMessage())
			return
		}
	}
	//Takes a string, and converts it to md5 hashed string.
	static String md5Hash(String message) {
		String md5 = ''
		if(message==null)
			return null

		message = message+salt//adding a salt to the string before it gets hashed.
		try {
			MessageDigest digest = MessageDigest.getInstance('MD5')//Create MessageDigest object for MD5
			digest.update(message.getBytes(), 0, message.length())//Update input string in message digest
			md5 = new BigInteger(1, digest.digest()).toString(16)//Converts message digest value in base 16 (hex)

		} catch (NoSuchAlgorithmException e) {
			println(e.printStackTrace())
		}
		return md5
	}

	static Map getKeys(){

		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance('RSA')
		keyPairGenerator.initialize(2048)
		KeyPair keyPair = keyPairGenerator.generateKeyPair()
		PrivateKey privateKey = keyPair.getPrivate()
		PublicKey publicKey = keyPair.getPublic()

		def keys = [:] as HashMap

		keys.put("private", privateKey)
		keys.put("public", publicKey)
		return keys
	}

	static String decryptMessage(String encryptedText,
			PrivateKey privateKey)  {
		Cipher cipher = Cipher.getInstance('RSA')
		cipher.init(Cipher.DECRYPT_MODE, privateKey)

		return new String(cipher.doFinal(Base64.getDecoder().decode(
				encryptedText)))
	}

	static String encryptMessage(String txt, PublicKey publicKey){
		Cipher cipher = Cipher.getInstance('RSA')
		cipher.init(Cipher.ENCRYPT_MODE, publicKey)
		return Base64.getEncoder().encodeToString(
				cipher.doFinal(txt.getBytes()))
	}

}
